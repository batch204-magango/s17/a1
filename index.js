// console.log("Hello");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function myDetails() {
		let fullName = prompt("What is your Full Name?");
		let age = prompt("How old are you?");
		let location = prompt("What is your location?") 

		console.log("Hello, " + fullName );
		console.log("Your are " + age + " years old");
		console.log("You live in " + location);

		alert("Welcome to my page!");
	}
	myDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topBand() {
		console.log("1: Silent Sanctuary");
		console.log("2: December Avenue");
		console.log("3: Parokya ni Edgar");
		console.log("4: Calalily");
		console.log("5: Juan");
	}
	topBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myMovies() {
		console.log("1: The Arrow");
		console.log("Rotten Tomatoes Rating: 97%")
		console.log("2: Ong Bak");
		console.log("Rotten Tomatoes Rating: 96% ")
		console.log("3: Transporter");
		console.log("Rotten Tomatoes Rating: 91% ")
		console.log("4: Hunger Games");
		console.log("Rotten Tomatoes Rating: 93% ")
		console.log("5: The Game of Thrones");
		console.log("Rotten Tomatoes Rating: 96% ")
	}
	myMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


// let printUsers() =
 function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

};


// console.log(friend1);
// console.log(friend2);

printUsers();